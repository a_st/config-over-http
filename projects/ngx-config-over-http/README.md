# NgxConfigOverHttp

[ ] You don't want to rebuild your application just to change the URL of your backend?

[ ] You don't want to build your application 10 times because different clients want different titles?

This very small library might help you.

## Installation

`ng add ngx-config-over-http`

## Usage

1. Edit the created *config.json*. All the content of the config.json will now be in the environment.
   Overriding duplicate keys!
2. (Recommended), add a onConfigReady to your AppComponent and only load components, when the environment is ready. You
   may skip this step if your entry Component doesn't require the environment. (But still not recommended)
3. (Optional) consider adding a service worker to cache the config.json
