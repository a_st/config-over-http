import {Rule, SchematicContext, Tree} from '@angular-devkit/schematics';
import {NodePackageInstallTask} from '@angular-devkit/schematics/tasks';

const SET_CONFIG_IMPORT = "import {setConfig} from 'ngx-config-over-http';\n";
const SET_CONFIG_STMT = "setConfig(environment, { configPath: 'assets/config/config.json', maxRetry: 3, waitUntilRetry: 100, debug: false });";
const SET_CONFIG_STMT_WITH_COMMENT = SET_CONFIG_STMT + " // Required for ngx-config-over-http-workspace"

// Just return the tree
export function ngAdd(): Rule {
  function modifyMainTS(mainTsContent: string, tree: Tree) {
    const indexOfPlatform = mainTsContent.lastIndexOf("platform") // hopefully that's the platformBrowserDynamic() or any similar thing
    // in theory if a blank line is \n and not \r\n or something weird like that, then this should work
    mainTsContent = SET_CONFIG_IMPORT + mainTsContent.substring(0, indexOfPlatform) + SET_CONFIG_STMT_WITH_COMMENT + "\n" + mainTsContent.substring(indexOfPlatform);
    tree.overwrite('./src/main.ts', mainTsContent);
  }

  return (tree: Tree, context: SchematicContext) => {
    context.addTask(new NodePackageInstallTask());
    let mainTsContent: string = tree.readText('./src/main.ts');

    if (mainTsContent.indexOf(SET_CONFIG_STMT) != -1) return tree; // cancel if already present

    modifyMainTS(mainTsContent, tree);
    if (!tree.exists('./src/assets/config/config.json')) {
      tree.create('./src/assets/config/config.json', "{}");
    }

    console.log("Make sure to use onConfigReady to prevent incomplete data.");
    return tree;
  };
}
