import {onConfigReady, setConfig} from './config-loader';
import {fakeAsync, flush, tick} from '@angular/core/testing';

describe('config loader', function () {

  const configOptions = {configPath: 'assets/config/config.json', maxRetry: 3, waitUntilRetry: 100, debug: true};

  it('should load variables into environment, overwriting existing ones, keeping old ones and adding new ones', fakeAsync(() => {
    let environment: any = {test: 'a', testB: 'b'};

    mockSuccessfulFetch({test: 'overwritten', testC: 'c'});
    setConfig(environment, configOptions);
    flush();
    expect(environment).toEqual({test: 'overwritten', testB: 'b', testC: 'c'});
  }));


  it('should call callback onConfigReady after setting Config', fakeAsync(() => {
    const onConfigReadyCallback = jasmine.createSpy('onConfigReadyCallback')
    let environment = {};
    mockSuccessfulFetch({test: 'test'});
    setConfig(environment, configOptions);
    onConfigReady(onConfigReadyCallback);
    expect(onConfigReadyCallback).not.toHaveBeenCalled();
    flush();
    expect(environment).toEqual({test: 'test'});
    expect(onConfigReadyCallback).toHaveBeenCalled();
  }));

  it('should call callback onConfigReady also after setConfig is done', fakeAsync(() => {
    const onConfigReadyCallback = jasmine.createSpy('onConfigReadyCallback')
    let environment = {};
    mockSuccessfulFetch({test: 'test'});
    setConfig(environment, configOptions);
    flush();
    expect(environment).toEqual({test: 'test'});
    onConfigReady(onConfigReadyCallback);
    expect(onConfigReadyCallback).toHaveBeenCalled(); // should call directly
  }));

  it('should retry the amount specified in the config if fetch fails', fakeAsync(() => {
    let environment: any = {test: 'a', testB: 'b'};
    const fetchSpy = mockUnsuccessfulFetch();
    setConfig(environment, {...configOptions, maxRetry: 2});
    tick(0);
    expect(fetchSpy).toHaveBeenCalledTimes(1);
    expect(() => tick(100)).toThrow();
    expect(fetchSpy).toHaveBeenCalledTimes(2);
    flush();
    expect(fetchSpy).toHaveBeenCalledTimes(2);
  }));

  it('should respect the waitUntilRetry option', fakeAsync( () => {
    let environment: any = {test: 'a', testB: 'b'};
    const fetchSpy = mockUnsuccessfulFetch();
    setConfig(environment, {...configOptions, waitUntilRetry: 100});
    tick(0);
    expect(fetchSpy).toHaveBeenCalledTimes(1);
    tick(50); // 50 < 100
    expect(fetchSpy).toHaveBeenCalledTimes(1);
    tick(100); // 50 + 100 > 100
    expect(fetchSpy).toHaveBeenCalledTimes(2);
    expect(() => flush()).toThrow();
  }));

});

function mockUnsuccessfulFetch() {
  return spyOn(window, 'fetch').and.returnValue(Promise.reject());
}

function mockSuccessfulFetch(response: any) {
  return spyOn(window, 'fetch').and.returnValue(Promise<Response>.resolve({json: () => response} as Response));
}
