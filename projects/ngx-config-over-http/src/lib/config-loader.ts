type ConfigOptions = { configPath: string, maxRetry: number, waitUntilRetry: number, debug: boolean };

function timeout(ms: number): Promise<void> {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function fetchConfig(configOptions: ConfigOptions, environment: any, currentTry = 0): Promise<void> {
  try {
    const response = await fetch(configOptions.configPath);
    configOptions.debug && console.log("NGX-CONFIG-OVER-HTTP", "Response fetched")
    const json = await response.json();
    configOptions.debug && console.log("NGX-CONFIG-OVER-HTTP", "Config deserialized");
    Object.assign(environment, json);
    configOptions.debug && console.log("NGX-CONFIG-OVER-HTTP", "Config loaded with environment:", environment);
    return;
  } catch (e) {
    configOptions.debug && console.error(`Failed to load config, currentTry: ${currentTry + 1}/${configOptions.maxRetry}`)
    if (configOptions.maxRetry > currentTry + 1) {
      await timeout(configOptions.waitUntilRetry);
      return fetchConfig(configOptions, environment, currentTry + 1);
    }
    configOptions.debug && console.error(`Failed to load config!`, e)
    throw e;
  }
}

/**
 * Loads config.json and overrides environment.ts values (use in main.ts!) And also wait in app component until onConfigReady
 */
export const setConfig = (environment: any, configOptions: ConfigOptions): void => {
  (window as any).configReadyPromise = new Promise<void>((resolve, reject) => {
    fetchConfig(configOptions, environment)
      .then(() => resolve())
      .catch(e => reject(e))
      .finally(() => {
        delete (window as any).configReadyPromise;
      })
  });
}

export const onConfigReady = (ready: () => void): void => {
  const configReadyPromise = (window as any).configReadyPromise as Promise<void>;
  if (!configReadyPromise) {
    ready();
  } else {
    configReadyPromise.then(() => ready());
  }
}
